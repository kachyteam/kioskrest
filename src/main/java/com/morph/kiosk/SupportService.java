/**
 * Class Name: SupportService Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Aug 28, 2016
 * 3:11:14 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk;

import com.morph.kiosk.model.ItemViewMode;
import com.morph.kiosk.model.KioskViewMode;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.Coupon;
import com.morph.kiosk.persistence.entity.Item;
import com.morph.kiosk.persistence.entity.ItemImage;
import com.morph.kiosk.persistence.entity.KioskItem;
import com.morph.kiosk.persistence.entity.ItemSubItem;
import com.morph.kiosk.persistence.service.PersistenceImp;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Path("/Support")
public class SupportService extends PersistenceImp {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    PortalPersistence persistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    UserPersistence userPersistence;

    @Inject
    HelperClass hc;

    @POST
    @Path("createBusiness")
    @Produces(MediaType.APPLICATION_JSON)
    public Response creatBusiness(@QueryParam("description") String description,
            @QueryParam("name") String name, @QueryParam("owner") String ownerId) {

        KioskUser owner = userPersistence.find(KioskUser.class, Long.valueOf(ownerId));

        Business business = new Business();
        business.setCreatedDate(new Date());
        business.setModifiedDate(new Date());
        business.setOwner(owner);
        business.setName(name);
        business.setDescription(description);

        persistence.create(business);
        return Response.ok(business).build();
    }

    @POST
    @Path("createKiosk")
    @Produces(MediaType.APPLICATION_JSON)
    public Response creatKiosk(@QueryParam("description") String description,
            @QueryParam("name") String name, @QueryParam("business") String businessId) {

        Business b = persistence.find(Business.class, Long.valueOf(businessId));

        Kiosk kiosk = new Kiosk();
        kiosk.setBusiness(b);
        kiosk.setCreatedDate(new Date());
        kiosk.setDescription(description);
        kiosk.setModifiedDate(new Date());
        kiosk.setName(name);
        kiosk.setStatus(true);

        persistence.create(kiosk);
        return Response.ok(kiosk).build();
    }

//    @POST
//    @Path("addSubItem")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response addSubItem(@QueryParam("mainItem") String mainItemId, @QueryParam("subItem") String subItemId) {
//
//        KioskItem mainItem = persistence.find(KioskItem.class, Long.valueOf(mainItemId));
//        KioskItem subItem = persistence.find(KioskItem.class, Long.valueOf(subItemId));
//
//        ItemSubItem mainitemSubitem = new ItemSubItem();
//        mainitemSubitem.setMainItem(mainItem);
//        mainitemSubitem.setSubItem(subItem);
//        persistence.create(mainitemSubitem);
//
//        //    mainItem.setSubItems(addToMainItem(mainItem));
//        return Response.ok(mainItem).build();
//    }

    @POST
    @Path("createItem")
    @Produces(MediaType.APPLICATION_JSON)
    public Response creatItem(@QueryParam("name") String name, @QueryParam("description") String description,
            @QueryParam("business") String businessId, @QueryParam("imageURL") String imageUrl) {

        Item item = new Item();
        item.setAvailable(Boolean.TRUE);
        item.setBusiness(persistence.find(Business.class, Long.valueOf(businessId)));
        item.setCreatedDate(new Date());
        item.setDescription(description);
        item.setModifiedDate(new Date());
        item.setName(name);
        item.setStatus(true);
        persistence.create(item);

        ItemImage itemImage = new ItemImage();
        itemImage.setImageUrl("/img" + imageUrl);
        itemImage.setItem(item);
        persistence.create(itemImage);

        return Response.ok(item).build();
    }

//    @POST
//    @Path("createKioskItem")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response creatKioskItem(@QueryParam("item") String itemId,
//            @QueryParam("kiosk") String kioskId, @QueryParam("price") String price) {
//
//        KioskItem item = new KioskItem();
//        item.setDateAdded(new Date());
//        item.setStatus(Boolean.TRUE);
//        item.setKiosk(persistence.find(Kiosk.class, Long.valueOf(kioskId)));
//        item.setItem(persistence.find(Item.class, Long.valueOf(itemId)));
//        persistence.create(item);
//
//        ItemPrice ip = new ItemPrice();
//        ip.setActive(Boolean.TRUE);
//        ip.setItem(item.getItem());
//
//        if (price != null) {
//            ip.setPrice(Double.valueOf(price));
//        }
//        persistence.create(ip);
//        return Response.ok(item).build();
//    }

    @GET
    @Path("getItemByBusiness")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ItemViewMode> getItemByBusiness(@QueryParam("businessId") String businessId) {
        List<Item> items = persistence.getAllItemByBusinessId(Long.valueOf(businessId));
        List<ItemViewMode> itemViewModes = new ArrayList<>();

        for (Item i : items) {
            itemViewModes.add(hc.itemToItemViewMode(i));
        }
        return itemViewModes;
    }

    @GET
    @Path("getKioskByBusiness")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KioskViewMode> getKioskByBusiness(@QueryParam("businessId") String businessId) {
        List<Kiosk> kiosks = persistence.getActiveKioskByBusiness(Long.valueOf(businessId));
        List<KioskViewMode> kioskViewModes = new ArrayList<>();

        for (Kiosk k : kiosks) {
            kioskViewModes.add(hc.kioskToKioskViewMode(k));
        }
        return kioskViewModes;
    }

    @GET
    @Path("getBusinessByOwner")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Business> getBusinessByUser(@QueryParam("ownerId") String ownerId) {
        List<Business> biz = persistence.geAllBusinessByUser(Long.valueOf(ownerId));
        return biz;
    }

    @GET
    @Path("readFile")
    @Produces(MediaType.APPLICATION_JSON)
    public String readFile(@QueryParam("url") String url) {
        BufferedReader br = null;
        System.out.println("JBoss Home: " + System.getProperty("jboss.server.data.dir"));
        String sCurrentLine = null;
        try {
            br = new BufferedReader(new FileReader(System.getProperty("jboss.server.data.dir") + url));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(KioskService.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }
        } catch (IOException ex) {
            Logger.getLogger(KioskService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sCurrentLine;
    }

//    @POST
//    @Path("createCoupon")
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response createCoupon(@QueryParam("percentage") Integer percentage, @QueryParam("code") String code,
//            @QueryParam("description") String description) {
//
//        Coupon c = new Coupon();
//        c.setPercentage(percentage);
//        c.setCode(code);
//        c.setDescription(description);
//        c.setGeneratedDate(new Date());
//        c.setValid(true);
//
//        persistence.create(c);
//        return Response.ok(c).build();
//    }

    @GET
    @Path("getBusinesses")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Business> getBusinesses() {
        List<Business> biz = persistence.geAllActiveBusiness();
        return biz;
    }
    
    @GET
    @Path("getAllKiosk")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KioskViewMode> getAllKiosk(@QueryParam("with") String with, @QueryParam("near") String near) {
        String lat = "";
        String lon = "";
        if (near != null) {
            String[] coordinate = near.split(",");
            lat = coordinate[0];
            lon = coordinate[1];
        }

        List<Kiosk> kiosks = persistence.getKiosks(Double.valueOf(lat), Double.valueOf(lon));
        kiosks.addAll(persistence.getKiosksByItemName(with));

        Set<Kiosk> s = new HashSet<>(kiosks);

        List<KioskViewMode> kioskViewModes = new ArrayList<>();

        for (Kiosk k : s) {
            kioskViewModes.add(hc.kioskToKioskViewMode(k));
        }
        return kioskViewModes;
    }

}
