/**
 * Class Name: KioskService Project Name: KioskTest-web Developer: Onyedika
 * Okafor (ookafor@morphinnovations.com) Version Info: Create Date: Jun 21, 2016
 * 4:12:26 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.morph.kiosk.email.EmailService;
import com.morph.kiosk.model.FinalOrder;
import com.morph.kiosk.model.Food;
import com.morph.kiosk.model.FoodVariation;
import com.morph.kiosk.model.InitializeResponse;
import com.morph.kiosk.model.ItemViewMode;
import com.morph.kiosk.model.KioskViewMode;
import com.morph.kiosk.model.OrderStatus;
import com.morph.kiosk.model.OrderSummary;
import com.morph.kiosk.model.OrderTray;
import com.morph.kiosk.model.PaystackInitializeResponse;
import com.morph.kiosk.model.PortalUser;
import com.morph.kiosk.model.TrayItem;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.Item;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskItem;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.PushRegister;
import com.morph.kiosk.persistence.entity.SubItem;
import com.morph.kiosk.persistence.entity.order.Cart;
import com.morph.kiosk.persistence.entity.order.Coupon;
import com.morph.kiosk.persistence.entity.order.CouponBatch;
import com.morph.kiosk.persistence.entity.order.CouponLog;
import com.morph.kiosk.persistence.entity.order.DeliveryAddress;
import com.morph.kiosk.persistence.entity.order.KioskDeliveryOption;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.entity.order.OrderedItem.Status;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption.Option;
import com.morph.kiosk.persistence.entity.order.OrderedItemStatusLog;
import com.morph.kiosk.persistence.entity.order.PaymentTransaction;
import com.morph.kiosk.persistence.entity.order.SubItemCart;
import com.morph.kiosk.persistence.entity.payment.PaymentProvider;
import com.morph.kiosk.persistence.entity.payment.Paystack;
import com.morph.kiosk.persistence.entity.portal.BusinessStaff;
import com.morph.kiosk.persistence.entity.portal.BusinessStaffRole;
import com.morph.kiosk.persistence.entity.portal.PasswordChange;
import com.morph.kiosk.persistence.service.PersistenceImp;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.inject.Inject;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Path("")
public class KioskService extends PersistenceImp {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    PortalPersistence persistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    UserPersistence userPersistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/EmailService!com.morph.kiosk.email.EmailService")
    private EmailService emailService;

    @Inject
    HelperClass hc;

    @POST
    @Path("createUser")
    @Produces(MediaType.APPLICATION_JSON)
    public Response creatUser(@QueryParam("firstname") String firstname, @QueryParam("middlename") String middlename,
            @QueryParam("surname") String surname, @QueryParam("password") String password,
            @QueryParam("emailAddress") String emailAddress, @QueryParam("phoneNo") String phoneNo) {

        KioskUser user = new KioskUser();

        if (HelperClass.isEmpty(firstname)) {
            return Response.status(400).entity(new StringBuilder(ProjectConstants.USER_FIRSTNAME_EMPTY)).build();
        }

        if (HelperClass.isEmpty(surname)) {
            return Response.status(400).entity(new StringBuilder(ProjectConstants.USER_SURNAME_EMPTY)).build();
        }

        if (HelperClass.isEmpty(phoneNo)) {
            return Response.status(400).entity(new StringBuilder(ProjectConstants.USER_PHONENO_EMPTY)).build();
        }

        if (HelperClass.isEmpty(password)) {
            return Response.status(400).entity(new StringBuilder(ProjectConstants.USER_PASSWORD_EMPTY)).build();
        }

        if (userPersistence.getKioskUserByEmailAddress(emailAddress) != null) {
            return Response.status(400).entity(new StringBuilder(ProjectConstants.USER_EMAILADDRESS_ALREADY_EXIST)).build();
        }

        if (userPersistence.getUserByPhoneNo(phoneNo) != null) {
            return Response.status(400).entity(new StringBuilder(ProjectConstants.USER_PHONE_NUMBER_ALREADY_EXIST)).build();
        }

        user.setCreatedDate(new Date());
        user.setModifiedDate(new Date());
        user.setEmailAddress(emailAddress);
        user.setFirstName(firstname);
        user.setMiddleName(middlename);
        user.setSurname(surname);
        user.setActive(true);
        user.setPhoneNumber(phoneNo);
        user.setPassword(userPersistence.hashPassword(user.getPassword()));
        user.setToken(HelperClass.generateToken());

        try {
            persistence.create(user);
        } catch (Exception e) {
            return Response.status(404).entity(new StringBuilder(ProjectConstants.ERROR_PERSISTING_DATA)).build();
        }

        return Response.ok(user).build();
    }

    @GET
    @Path("updateUserPassword")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserPassword(@QueryParam("validationCode") String validationCode,
            @QueryParam("newPassword") String newPassword) {
        if (HelperClass.isEmpty(validationCode)) {
            return Response.status(404).entity(new StringBuilder("Validation Code is Empty")).build();
        }

        if (HelperClass.isEmpty(newPassword)) {
            return Response.status(404).entity(new StringBuilder(ProjectConstants.USER_PASSWORD_EMPTY)).build();
        }

        PasswordChange pc = persistence.getPasswordChangeByCode(validationCode);
        if (pc == null) {
            return Response.status(404).entity(new StringBuilder("Validation Code is not Valid")).build();
        }

        if (new Date().getTime() > pc.getRequestExpirationTime().getTime()) {
            return Response.status(404).entity(new StringBuilder("Validation Code has Expired")).build();
        }

        if (pc.getCompletedTime() != null) {
            return Response.status(404).entity(new StringBuilder("Validation Code has been used")).build();
        }

        KioskUser ku = pc.getKioskUser();
        ku.setPassword(userPersistence.hashPassword(newPassword));
        ku.setModifiedDate(new Date());
        persistence.update(ku);

        pc.setCompletedTime(new Date());
        persistence.update(pc);

        return Response.ok().build();
    }

    @GET
    @Path("recoverAccount")
    @Produces(MediaType.APPLICATION_JSON)
    public Response recoverUserAccount(@QueryParam("email") String email) {
        if (HelperClass.isEmpty(email)) {
            return Response.status(404).entity(new StringBuilder(ProjectConstants.USER_EMAILADDRESS_EMPTY)).build();
        }

        KioskUser ku = userPersistence.getKioskUserByEmailAddress(email);
        if (ku == null) {
            return Response.status(400).entity(new StringBuilder(ProjectConstants.USER_EMAILADDRESS_EMPTY)).build();
        }

        String code = UUID.randomUUID().toString().substring(0, 8).toUpperCase();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 1);
        Date expireDate = cal.getTime();

        PasswordChange pc = new PasswordChange();
        pc.setChangeCode(code);
        pc.setKioskUser(ku);
        pc.setRequestTime(new Date());
        pc.setRequestExpirationTime(expireDate);

        persistence.create(pc);

        String message = "Dear " + ku.getFirstName() + " " + ku.getSurname() + "  <br />"
                + "You requested for a password reset in your Chef Tent account. "
                + "If this was a mistake, just ignore this email and nothing will happen. To reset your password,"
                + "copy the code below into your mobile device. This is code is valid for 24 hours: "
                + code
                + "<br />"
                + "Thank you!";
        emailService.prepareEmail(message, ProjectConstants.PASSWORD_CHARGE_SUBJECT, ku.getEmailAddress().trim(), null, null);

        return Response.ok().build();
    }

    @GET
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@QueryParam("emailAddress") String emailAddress, @QueryParam("password") String password) {

        if (HelperClass.isEmpty(emailAddress)) {
            return Response.status(404).entity(new StringBuilder(ProjectConstants.USER_EMAILADDRESS_EMPTY)).build();
        }

        if (HelperClass.isEmpty(password)) {
            return Response.status(404).entity(new StringBuilder(ProjectConstants.USER_PASSWORD_EMPTY)).build();
        }

        KioskUser currentUser = userPersistence.login(emailAddress, password);
        if (currentUser == null) {
            return Response.status(404).entity(new StringBuilder(ProjectConstants.USER_DOES_NOT_EXIST)).build();
        }

        BusinessStaff bs = persistence.getBusinessStaffByKioskUser(currentUser);

        if (bs != null) {
            PortalUser pu = new PortalUser();
            pu.setActive(currentUser.isActive());
            pu.setCreatedDate(currentUser.getCreatedDate());
            pu.setDesignation(currentUser.getDesignation());
            pu.setEmailAddress(currentUser.getEmailAddress());
            pu.setFirstName(currentUser.getFirstName());
            pu.setId(currentUser.getId());
            pu.setMailingAddress(currentUser.getMailingAddress());
            pu.setMiddleName(currentUser.getMiddleName());
            pu.setModifiedDate(currentUser.getModifiedDate());
            pu.setPassword(currentUser.getPassword());
            pu.setPhoneNumber(currentUser.getPhoneNumber());
            pu.setSurname(currentUser.getSurname());
            pu.setToken(currentUser.getToken());

            if (currentUser.getDesignation() == KioskUser.Designation.STAFF) {
                pu.setDesignation(currentUser.getDesignation());
            }
            if (currentUser.getDesignation() == null && persistence.getPortalUserByKioskUser(currentUser) != null) {
                pu.setDesignation(KioskUser.Designation.BUSINESSOWNER);
            }

            List<BusinessStaffRole> bsrs = persistence.getBusinessStaffRoleByBusinessStaff(bs);

            if (!bsrs.isEmpty()) {
                for (BusinessStaffRole bsr : bsrs) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(bsr.getKiosk().getId());
                    sb.append("-");
                    sb.append(bsr.getUserGroup().getUserGroupName());
                    pu.getPermission().add(sb.toString());
                }
                return Response.ok(pu).build();
            }
        }
        return Response.ok(currentUser).build();
    }

    @GET
    @Path("getAllKitchen")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KioskViewMode> getAllKitchen(@QueryParam("with") String with) {
        List<Kiosk> kiosks;
        List<Kiosk> kitchens = new ArrayList<>();
        if (!HelperClass.isEmpty(with)) {
            List<KioskItem> kis = persistence.getKioskItemByName(with);
            for (KioskItem ki : kis) {
                if (ki.getKiosk().getBusiness().isStatus()) {
                    if (ki.getKiosk().isStatus()) {
                        if (ki.getKiosk().getType().equals(Kiosk.KioskType.KITCHEN)) {
                            kitchens.add(ki.getKiosk());
                        }
                    }
                }
            }
        } else {
            kiosks = persistence.getAllKitchen();
            for (Kiosk k : kiosks) {
                if (k.getType().equals(Kiosk.KioskType.KITCHEN)) {
                    List<KioskItem> kioskItems = persistence.getKioskItemByKioskId(k.getId());
                    if (!kioskItems.isEmpty()) {
                        kitchens.add(k);
                    }
                }
            }
        }

        Set<Kiosk> filteredKitchen = new HashSet<>(kitchens);
        List<KioskViewMode> kioskViewModes = new ArrayList<>();

        for (Kiosk k : filteredKitchen) {
            kioskViewModes.add(hc.kioskToKioskViewMode(k));
        }
        return kioskViewModes;
    }

    @GET
    @Path("getAllBusiness")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KioskViewMode> getAllBusiness(@QueryParam("with") String with, @QueryParam("near") String near) {
        String lat = "";
        String lon = "";
        if (near != null) {
            String[] coordinate = near.split(",");
            lat = coordinate[0];
            lon = coordinate[1];
        }

        List<Kiosk> kiosks = persistence.getKiosks(Double.valueOf(lat), Double.valueOf(lon));
        List<KioskViewMode> kioskViewModes = new ArrayList<>();
        if (!HelperClass.isEmpty(with)) {
            List<KioskItem> kis = persistence.getKioskItemByName(with);
            for (KioskItem ki : kis) {
                kiosks.add(ki.getKiosk());
            }
        }
        Set<Kiosk> filteredKiosk = new HashSet<>(kiosks);
        Set<Business> filteredBusiness = new HashSet<>();

        for (Kiosk k : filteredKiosk) {
            if (!k.getBusiness().isStatus()) {
                continue;
            }
            if (!k.isStatus()) {
                continue;
            }
            if (k.getType().equals(Kiosk.KioskType.KITCHEN)) {
                continue;
            }

            List<KioskItem> kioskItems = persistence.getKioskItemByKioskId(k.getId());
            if (kioskItems.isEmpty()) {
                continue;
            }
            filteredBusiness.add(k.getBusiness());
        }

        for (Business b : filteredBusiness) {
            kioskViewModes.add(hc.businessToBusinessViewMode(b));
        }
        return kioskViewModes;
    }

    @GET
    @Path("getItemsByKiosk")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ItemViewMode> getItemsByKiosk(@QueryParam("kioskId") Long kioskId) {
        List<KioskItem> items = persistence.getKioskItemByKioskId(kioskId);
        Set<KioskItem> items1 = new HashSet<>(items);
        List<ItemViewMode> itemzz = new ArrayList<>();

        for (KioskItem ki : items1) {
            ItemViewMode itemViewMode = hc.kioskItemToItemViewMode(ki);
            itemzz.add(itemViewMode);
        }

        return itemzz;
    }

    @GET
    @Path("getKioskItemByKioskAndItem")
    @Produces(MediaType.APPLICATION_JSON)
    public ItemViewMode getKioskItemByKioskAndItem(@QueryParam("kioskId") Long kioskId, @QueryParam("itemId") Long itemId) {
        KioskItem ki = persistence.getKioskItemByKioskAndItem(kioskId, itemId);
        ItemViewMode itemViewMode = new ItemViewMode();
        if (ki != null) {
            itemViewMode = hc.kioskItemToItemViewMode(ki);
        }
        return itemViewMode;
    }

    @GET
    @Path("getItemsByBusiness")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ItemViewMode> getItemsByBusiness(@QueryParam("businessId") Long businessId, @QueryParam("near") String near) {
        String lat = "";
        String lon = "";
        if (near != null) {
            String[] coordinate = near.split(",");
            lat = coordinate[0];
            lon = coordinate[1];
        }

        Business b = persistence.find(Business.class, businessId);

        List<Kiosk> kiosks = persistence.getKiosks(Double.valueOf(lat), Double.valueOf(lon));

        List<Kiosk> nearKiosks = new ArrayList<>();
        List<KioskItem> items = new ArrayList<>();

        for (Kiosk k1 : kiosks) {
            if (k1.isStatus()) {
                if (k1.getBusiness().equals(b)) {
                    nearKiosks.add(k1);
                }
            }

        }

        for (Kiosk k1 : nearKiosks) {
            items.addAll(persistence.getKioskItemByKioskId(k1.getId()));
        }

        List<ItemViewMode> itemzz = new ArrayList<>();
        Set<KioskItem> kioskItems = new HashSet<>(items);

        for (KioskItem ki : kioskItems) {
            ItemViewMode itemViewMode = hc.kioskItemToItemViewMode(ki);

            List<SubItem> subItems = persistence.getItemSubItems(ki.getItem());
            List<ItemViewMode> itemViewModes = new ArrayList<>();
            for (SubItem ki1 : subItems) {
                ItemViewMode itemViewMode1 = hc.subItemToItemViewMode(ki1);
                itemViewModes.add(itemViewMode1);
            }

            itemViewMode.setSubItems(itemViewModes);
            itemzz.add(itemViewMode);
        }

        return itemzz;
    }

    @GET
    @Path("getDeliveryOptions")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KioskDeliveryOption> getAllDeliveryOption(@QueryParam("kioskId") Long kioskId) {
        List<KioskDeliveryOption> options = persistence.getKioskDeliveryOptionsByKioskId(kioskId);
        return options;
    }

    @GET
    @Path("getPaymentOptions")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KioskPaymentOption> getAllPaymentOptions(@QueryParam("kioskId") Long kioskId) {
        List<KioskPaymentOption> options = persistence.getKioskPaymentOptionsByKioskId(kioskId);
        return options;
    }

    @GET
    @Path("searchByItem")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KioskItem> searchByItem(@QueryParam("item") String item) {
        List<KioskItem> kioskItems = new ArrayList<>();
        Set<KioskItem> s = new HashSet<>(persistence.getKioskItemByName(item));
        kioskItems.addAll(s);
        return kioskItems;
    }

    @GET
    @Path("myDeliveryAddresses")
    @Produces(MediaType.APPLICATION_JSON)
    public List<DeliveryAddress> myDeliveryAddresses(@QueryParam("token") String token) {
        KioskUser kioskUser = persistence.getUserByToken(token);
        List<DeliveryAddress> addresses = new ArrayList<>();
        Set<DeliveryAddress> s = new HashSet<>();
        for (OrderedItem oi : persistence.myOrders(kioskUser)) {
            if (oi.getDeliveryAddress() != null) {
                s.add(oi.getDeliveryAddress());
            }
        }

        addresses.addAll(s);
        return addresses;
    }

    @GET
    @Path("validateCoupon")
    @Produces(MediaType.APPLICATION_JSON)
    public Response isCouponValid(@QueryParam("coupon") String coupon, @QueryParam("kioskId") Long kioskId,
            @QueryParam("token") String token) {
        boolean valid = false;
        Map<String, String> resp = new HashMap<>();
        Kiosk k = persistence.find(Kiosk.class, kioskId);
        KioskUser ku = persistence.getUserByToken(token);
        if (ku != null) {
            if (k != null) {
                valid = persistence.isCouponValid(coupon, k.getBusiness().getId(), ku.getId());
            }
        } else {
            resp.put("valid", "No Token");
        }

        if (valid) {
            Coupon c = persistence.getCouponByCode(coupon);
            resp.put("description", c.getCouponBatch().getDescription());
            resp.put("valid", "true");
            resp.put("code", coupon);
            resp.put("expire", c.getCouponBatch().getDateExpired().toString());
        } else {
            resp.put("valid", "false");
        }
        return Response.ok(resp).build();
    }

    @POST
    @Path("makeTray")
    @Produces(MediaType.APPLICATION_JSON)
    public OrderTray makeTray(OrderTray tray) {
        OrderedItem item = new OrderedItem();
        item.setOrderStatus(Status.INCOMPLETE);
        persistence.create(item);

        TrayItem ti = tray.getItems().get(0);
        if (ti != null) {
            KioskItem ki = persistence.find(KioskItem.class, ti.getFood().getId());
            tray.setKiosk(ki.getKiosk().getId());
            tray.setPlace(ki.getKiosk());
        }

        tray.setId(item.getId());

        return tray;
    }

    @POST
    @Path("orderItem")
    @Produces(MediaType.APPLICATION_JSON)
    public Response orderItem(OrderTray tray) {
        OrderSummary orderSummary = new OrderSummary();
        if (tray != null) {
            if (tray.getId() != null) {
                orderSummary = hc.processOrder(tray);
            }
        }
        return Response.ok(orderSummary).build();
    }

    @POST
    @Path("preparePayment")
    @Produces(MediaType.APPLICATION_JSON)
    public Response preparePayment(OrderSummary orderSummary, @QueryParam("token") String token,
            @QueryParam("phone") String phone, @QueryParam("guestId") String guestId, @QueryParam("firstname") String firstname,
            @QueryParam("lastname") String lastname) {

        OrderedItem oi = persistence.find(OrderedItem.class, orderSummary.getOrderId());
        if (oi != null) {
            oi.setCheckedOutDate(new Date());
            if (token != null) {
                oi.setOrderedBy(persistence.getUserByToken(token));
                persistence.update(oi);
                DeliveryAddress da = oi.getDeliveryAddress();
                if (da != null) {
                    da.setPhoneNo(oi.getOrderedBy().getPhoneNumber());
                    da.setEmailAddress(oi.getOrderedBy().getEmailAddress());
                    persistence.update(da);
                }
            } else {
                if (HelperClass.isEmpty(phone)) {
                    return Response.status(404).entity(new StringBuilder(ProjectConstants.USER_PHONENO_EMPTY)).build();
                }
                DeliveryAddress da = oi.getDeliveryAddress();
                if (da != null) {
                    da.setPhoneNo(phone);
                    persistence.update(da);
                }

                KioskUser kioskUser = persistence.getUserByToken(guestId.trim());
                if (kioskUser != null) {
                    kioskUser.setFirstName(firstname);
                    kioskUser.setSurname(lastname);
                    kioskUser.setModifiedDate(new Date());
                    kioskUser.setPhoneNumber(phone);
                    persistence.update(kioskUser);
                } else {
                    KioskUser k = userPersistence.getUserByPhoneNo(phone);
                    if(k == null){
                        kioskUser = new KioskUser();
                        kioskUser.setActive(true);
                        kioskUser.setFirstName(firstname);
                        kioskUser.setSurname(lastname);
                        kioskUser.setCreatedDate(new Date());
                        kioskUser.setModifiedDate(new Date());
                        kioskUser.setPhoneNumber(phone);
                        kioskUser.setToken(guestId);
                        persistence.create(kioskUser);
                    }else{
                        k.setFirstName(firstname);
                        k.setSurname(lastname);
                        k.setModifiedDate(new Date());
                        k.setToken(guestId);
                        persistence.update(k);
                    }
                }
                oi.setOrderedBy(kioskUser);
                persistence.update(oi);
            }

            KioskPaymentOption option = persistence.find(KioskPaymentOption.class, Long.valueOf(orderSummary.getPaymentOption()));
            oi.setPaymentOption(option);
            oi.setPaymentMethod(option.getPaymentOption());
            persistence.update(oi);
            InitializeResponse ir = new InitializeResponse();

            if (option.getPaymentOption().getOption().equalsIgnoreCase(KioskPaymentOption.Option.ONLINE.getOption())) {

                PaymentProvider pp = persistence.getEnabledPaymentProvider();

                String paymentProviderClassname;
                try {
                    paymentProviderClassname = pp.getClassName();
                } catch (NullPointerException npe) {
                    return Response.status(404).entity(new StringBuilder("Could not retrieve Payment Provider")).build();
                }

                if (paymentProviderClassname.equalsIgnoreCase(ProjectConstants.PAYSTACK)) {
                    try {
                        ir = hc.payStackPayment(ir, oi, pp);
                    } catch (Exception e) {
                        e.printStackTrace();
                        ir.setMessage("Error Preparing Payment");
                        ir.setResponseCode("00");
                        return Response.ok(ir).build();
                    }
                }
            } else if (option.getPaymentOption().getOption().equalsIgnoreCase(KioskPaymentOption.Option.PAYONDELIVERY.getOption())) {
                oi.setCheckedOutDate(new Date());
                oi.setLastModifiedDate(new Date());
                oi.setOrderStatus(Status.PENDING);
                persistence.update(oi);

                Coupon c = persistence.getCouponByCode(orderSummary.getCouponCode());
                if (c != null) {
                    c.setDateUsed(new Date());
                    c.setUsed(persistence.isUsageExceeded(c.getCode()));
                    persistence.update(c);

                    CouponLog cl = persistence.getCouponLogByOrderIdAndCouponId(oi.getId(), c.getId());
                    cl.setUserId(oi.getOrderedBy().getId());
                    persistence.update(cl);
                }

                ir.setMessage("Available");
                ir.setResponseCode("01");

                OrderedItemStatusLog log = persistence.getLogByOrderItemAndStatus(oi, Status.PENDING.getStatus());
                if (log == null) {
                    log = new OrderedItemStatusLog();
                    log.setCreatedDate(new Date());
                    log.setItem(oi);
                    log.setLastModifiedDate(new Date());
                    log.setOrderStatus(Status.PENDING);
                    persistence.create(log);
                }

                PaymentTransaction pt = persistence.getPaymentTransactionByOrder(oi);
                if (pt == null) {
                    pt = new PaymentTransaction();
                    pt.setAmount(oi.getTotalAmount());
                    pt.setAmountBeforeCharge(oi.getTotalAmount());
                    pt.setPaymentPlatformCharge(0.0);
                    pt.setOrderItem(oi);
                    pt.setPayer(oi.getOrderedBy());
                    pt.setTractionStatus(PaymentTransaction.PaymentTractionStatus.PAYMENTPENDING);
                    persistence.create(pt);
                }

                hc.pushNotifyForNewOrder(oi);
                return Response.ok(ir).build();
            }
            return Response.ok(ir).build();
        } else {
            return Response.status(404).entity(new StringBuilder("Could not retrieve Order")).build();
        }
    }

    @GET
    @Path("paystackPostPaymentResponse")
    @Produces(MediaType.APPLICATION_JSON)
    public Response paystackPostPaymentResponse(@QueryParam("trxref") String trxref) {

        PaymentTransaction pt = persistence.getPaymentTransactionByTrxRef(trxref);
        if (pt != null) {
            Paystack ps = persistence.find(Paystack.class, pt.getPaymentProvider().getProviderId());
            String secretKey = ps.getSecretKey();

            PaystackInitializeResponse pir = hc.verifyPayment(trxref, secretKey);

            if (pir == null) {
                pt.setTractionStatus(PaymentTransaction.PaymentTractionStatus.PAYMENTFAILED);
                persistence.update(pt);
            } else {
                if (pir.isStatus()) {
                    pt.setAuthCode(pir.getData().getAuthorization().getAuthorization_code());
                    pt.setTractionStatus(PaymentTransaction.PaymentTractionStatus.PAYMENYSUCCESSFUL);
                    pt.setTranxDate(new Date());
                    persistence.update(pt);

                    OrderedItem oi = pt.getOrderItem();
                    Coupon c = persistence.getCouponByCode(oi.getPromoCode());
                    if (c != null) {
                        c.setDateUsed(new Date());
                        c.setUsed(persistence.isUsageExceeded(c.getCode()));
                        persistence.update(c);

                        CouponLog cl = persistence.getCouponLogByOrderIdAndCouponId(oi.getId(), c.getId());
                        cl.setUserId(oi.getOrderedBy().getId());
                        persistence.update(cl);
                    }

                    oi.setOrderStatus(Status.PENDING);
                    oi.setLastModifiedDate(new Date());
                    oi.setCheckedOutDate(new Date());
                    persistence.update(oi);

                    OrderedItemStatusLog log = new OrderedItemStatusLog();
                    log.setCreatedDate(new Date());
                    log.setItem(oi);
                    log.setLastModifiedDate(new Date());
                    log.setOrderStatus(Status.PENDING);
                    persistence.create(log);

                    hc.pushNotifyForNewOrder(oi);
                } else {
                    pt.setAuthCode(pir.getData().getAuthorization().getAuthorization_code());
                    pt.setTractionStatus(PaymentTransaction.PaymentTractionStatus.PAYMENTFAILED);
                    pt.setTranxDate(new Date());
                    persistence.update(pt);
                }
            }
        }
        return Response.temporaryRedirect(URI.create("http://morphinnovations.com")).build();
    }

    @POST
    @Path("cancelOrder")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelOrder(@QueryParam("token") String token, @QueryParam("message") String message,
            @QueryParam("orderId") Long orderId) {
        FinalOrder fo = new FinalOrder();
        OrderedItem item = persistence.find(OrderedItem.class, orderId);
        if (item != null) {
            item.setLastModifiedBy(persistence.getUserByToken(token));
            item.setLastModifiedDate(new Date());
            item.setOrderStatus(OrderedItem.Status.CANCELLED);
            persistence.update(item);

            OrderedItemStatusLog log = new OrderedItemStatusLog();
            log.setComment(message);
            log.setCreatedDate(new Date());
            log.setItem(persistence.find(OrderedItem.class, orderId));
            log.setKiosk(item.getKiosk());
            log.setLastModifiedDate(new Date());
            log.setOrderStatus(OrderedItem.Status.CANCELLED);
            log.setUpdatedby(persistence.getUserByToken(token));
            persistence.create(log);
            fo = hc.finalOrderFromOrderItem(item);
            return Response.ok(fo).build();
        }
        return Response.ok(fo).build();
    }

    @POST
    @Path("customerOrderConfirmation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response customerOrderConfirmation(@QueryParam("token") String token, @QueryParam("message") String message,
            @QueryParam("orderId") Long orderId) {

        OrderedItem item = persistence.find(OrderedItem.class, orderId);
        item.setLastModifiedBy(persistence.getUserByToken(token));
        item.setLastModifiedDate(new Date());
        item.setOrderStatus(OrderedItem.Status.RECEIVED);
        persistence.update(item);

        OrderedItemStatusLog log = new OrderedItemStatusLog();
        log.setComment(message);
        log.setCreatedDate(new Date());
        log.setItem(persistence.find(OrderedItem.class, orderId));
        log.setKiosk(item.getKiosk());
        log.setLastModifiedDate(new Date());
        log.setOrderStatus(OrderedItem.Status.RECEIVED);
        log.setUpdatedby(persistence.getUserByToken(token));
        persistence.create(log);
        FinalOrder fo = hc.finalOrderFromOrderItem(item);
        return Response.ok(fo).build();
    }

    @POST
    @Path("manageOrder")
    @Produces(MediaType.APPLICATION_JSON)
    public Response manageOrder(@QueryParam("stage") String stage,
            @QueryParam("token") String token, @QueryParam("message") String message,
            @QueryParam("orderId") Long orderId, @QueryParam("recipient") Long recipient) {
        OrderedItem item = persistence.find(OrderedItem.class, orderId);
        KioskUser modUser = persistence.getUserByToken(token);
        Status inputStatus = Status.valueOf(stage.toUpperCase());

        if (inputStatus.equals(OrderedItem.Status.SHIPPED) && item.getDeliveryOption().getDeliveryOption().equals(KioskDeliveryOption.Option.PICKUP)) {
            hc.managePickup(item, modUser, message);
        }

        if (inputStatus.equals(OrderedItem.Status.CANCELLED)) {
            hc.cancelOrder(item, modUser, message);
        }

        int currentStatusIndex = item.getOrderStatus().ordinal();
        OrderedItem.Status nextStatus = null;
        for (OrderedItem.Status s : OrderedItem.Status.values()) {
            if (s.ordinal() == currentStatusIndex + 1) {
                nextStatus = s;
                break;
            }
        }

        if (nextStatus != inputStatus) {
            return Response.status(400).entity(new StringBuilder("This Stage already executed")).build();
        }

        item.setLastModifiedBy(modUser);
        item.setLastModifiedDate(new Date());
        item.setOrderStatus(nextStatus);
        persistence.update(item);

        OrderedItemStatusLog log = new OrderedItemStatusLog();
        log.setComment(message);
        log.setCreatedDate(new Date());
        log.setItem(item);
        log.setKiosk(item.getKiosk());
        log.setLastModifiedDate(new Date());
        log.setOrderStatus(nextStatus);
        log.setUpdatedby(modUser);

        hc.notifications(nextStatus, item, log, recipient);
        persistence.create(log);
        FinalOrder fo = hc.finalOrderFromOrderItem(item);

        return Response.ok(fo).build();
    }

    @POST
    @Path("updateOfflinePayment")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateOfflinePayment(@QueryParam("paymentType") String paymentType, @QueryParam("token") String token,
            @QueryParam("orderId") Long orderId) {
        OrderedItem oi = persistence.find(OrderedItem.class, orderId);
        PaymentTransaction pt = persistence.getPaymentTransactionByOrder(oi);
        KioskUser ku = persistence.getUserByToken(token);

        if (oi != null && pt != null) {
            Option option = Option.valueOf(paymentType.toUpperCase());

            oi.setLastModifiedBy(ku);
            oi.setLastModifiedDate(new Date());
            oi.setPaymentMethod(option);
            persistence.update(oi);

            pt.setTractionStatus(PaymentTransaction.PaymentTractionStatus.PAYMENYSUCCESSFUL);
            pt.setTranxDate(new Date());
            pt.setTransactionRef(UUID.randomUUID().toString());
            persistence.update(pt);

            return Response.ok(new StringBuilder("Update Successful")).build();
        }

        return Response.ok(new StringBuilder("Update Not Successful")).build();
    }

    @GET
    @Path("orderStatusList")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Status> orderStatusList() {
        List<Status> ses = Arrays.asList(Status.values());
        return ses;
    }

    @GET
    @Path("ordersByKiosk")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FinalOrder> ordersByKiosk(@QueryParam("token") String token, @QueryParam("stage") String stage, @QueryParam("fromDate") Long fromDate) {
        KioskUser ku = persistence.getUserByToken(token);
        List<Kiosk> ks = persistence.getActiveKioskByUser(ku);
        List<OrderedItem> items = new ArrayList<>();
        Date date = null;
        if (fromDate != null) {
            date = new Date(fromDate);
        }

        if (!ks.isEmpty()) {
            List<OrderedItem> itemz = new ArrayList<>();
            for (Kiosk k : ks) {
                if (date != null) {
                    itemz.addAll(persistence.getOrderedItemByKiosk(k, date));
                } else {
                    itemz.addAll(persistence.getOrderedItemByKiosk(k));
                }
            }
            if (stage != null && !itemz.isEmpty()) {
                for (OrderedItem item : itemz) {
                    if (item.getOrderStatus().equals(Status.valueOf(stage.toUpperCase()))) {
                        items.add(item);
                    }
                }
            }
        } else {
            if (stage != null) {
                if (Status.valueOf(stage.toUpperCase()).equals(Status.RECEIVED) || Status.valueOf(stage.toUpperCase()).equals(Status.PENDING)) {
                    List<BusinessStaffRole> bsrs = persistence.getUserRole(ku, ProjectConstants.ATTENDANT);
                    List<Kiosk> mKiosks = new ArrayList<>();
                    for (BusinessStaffRole bsr : bsrs) {
                        mKiosks.add(bsr.getKiosk());
                    }
                    List<OrderedItem> itemz = new ArrayList<>();
                    for (Kiosk k : mKiosks) {
                        if (date != null) {
                            itemz.addAll(persistence.getOrderedItemByKiosk(k, date));
                        } else {
                            itemz.addAll(persistence.getOrderedItemByKiosk(k));
                        }
                    }

                    for (OrderedItem item : itemz) {
                        if (item.getOrderStatus().equals(Status.valueOf(stage.toUpperCase()))) {
                            items.add(item);
                        }
                    }
                }

                if (Status.valueOf(stage.toUpperCase()).equals(Status.PROCESSING)) {
                    List<BusinessStaffRole> bsrs = persistence.getUserRole(ku, ProjectConstants.LOGISTICS_MANAGER);
                    List<Kiosk> mKiosks = new ArrayList<>();
                    for (BusinessStaffRole bsr : bsrs) {
                        mKiosks.add(bsr.getKiosk());
                    }
                    List<OrderedItem> itemz = new ArrayList<>();
                    for (Kiosk k : mKiosks) {
                        if (date != null) {
                            itemz.addAll(persistence.getOrderedItemByKiosk(k, date));
                        } else {
                            itemz.addAll(persistence.getOrderedItemByKiosk(k));
                        }
                    }

                    for (OrderedItem item : itemz) {
                        if (item.getOrderStatus().equals(Status.valueOf(stage.toUpperCase()))) {
                            items.add(item);
                        }
                    }
                }

                if (Status.valueOf(stage.toUpperCase()).equals(Status.SHIPPED)) {
                    items.addAll(persistence.getOrderedItemByStage(ku, Status.SHIPPED));
                }

                if (Status.valueOf(stage.toUpperCase()).equals(Status.DELIVERED)) {
                    List<OrderedItem> itemz = new ArrayList<>();
                    itemz.addAll(persistence.getOrderedItemByStage(ku, Status.SHIPPED));
                    for (OrderedItem item : itemz) {
                        if (item.getOrderStatus().equals(Status.DELIVERED)) {
                            items.add(item);
                        }
                    }
                }
            }
        }

        Set<OrderedItem> items1 = new HashSet<>(items);
        List<FinalOrder> oses = new ArrayList<>();

        items1.stream()
                .forEach((oi) -> {
                    oses.add(hc.finalOrderFromOrderItem(oi));
                }
                );
        return oses;

    }

    @GET
    @Path("myOrder")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FinalOrder> myOrder(@QueryParam("token") String token) {
        KioskUser k = persistence.getUserByToken(token);
        List<FinalOrder> oses = new ArrayList<>();
        for (OrderedItem oi : persistence.myOrders(k)) {
            oses.add(hc.finalOrderFromOrderItem(oi));
        }
        return oses;
    }

    @GET
    @Path("orderTimeline")
    @Produces(MediaType.APPLICATION_JSON)
    public List<OrderStatus> myOrder(@QueryParam("orderId") Long orderId) {

        List<OrderedItemStatusLog> statusLogs = persistence.getOrderLog(orderId);
        List<OrderStatus> os = new ArrayList<>();
        statusLogs.stream().map((log) -> {
            OrderStatus os1 = new OrderStatus();
            os1.setComment(log.getComment());
            os1.setId(log.getId());
            os1.setLastModifiedDate(log.getLastModifiedDate());
            os1.setStatus(log.getOrderStatus().getStatus());
            if (log.getUpdatedby() != null) {
                os1.setUpdatedBy(log.getUpdatedby().getFirstName() + " " + log.getUpdatedby().getSurname());
            }
            return os1;
        }).forEach((os1) -> {
            os.add(os1);
        });

        return os;
    }

    @POST
    @Path("registerGCM")
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerGCM(@QueryParam("token") String token,
            @QueryParam("regId") String regId, @QueryParam("platform") String platform) {

        KioskUser ku = persistence.getUserByToken(token);
        if (ku == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        PushRegister pr = persistence.getPushRegisterByKioskUser(ku);

        if (pr != null) {
            pr.setRegId(regId);
            persistence.update(pr);
        } else {
            pr = new PushRegister();
            pr.setRegId(regId);
            pr.setUserId(persistence.getUserByToken(token));
            for (PushRegister.Platform s : PushRegister.Platform.values()) {
                if (s.getPlatform().equalsIgnoreCase(platform)) {
                    pr.setPlatform(s);
                }
            }
            persistence.create(pr);
        }
        return Response.ok(pr).build();
    }

    @GET
    @Path("getKioskDispatcher")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getKioskDispatcher(@QueryParam("token") String token, @QueryParam("kioskId") String kioskId) {
        List<KioskUser> dispatchers = new ArrayList<>();
        Kiosk k = persistence.find(Kiosk.class, Long.valueOf(kioskId));
        if (k != null) {
            dispatchers.addAll(persistence.getUserByKioskAndUsergroup(k, ProjectConstants.DISPATCH_RIDER));
        }
        return Response.ok(dispatchers).build();
    }

    @GET
    @Path("getUserIdList")
    @Produces(MediaType.APPLICATION_JSON)
    public List<PushRegister> getUserIdList() {
        List<PushRegister> ses = persistence.findWithQuery("SELECT o FROM PushRegister o");
        return ses;
    }

    @GET
    @Path("reorder")
    @Produces(MediaType.APPLICATION_JSON)
    public OrderTray reorder(@QueryParam("orderId") Long orderId) {
        OrderedItem oi = persistence.find(OrderedItem.class, orderId);

        OrderedItem item = new OrderedItem();
        item.setOrderStatus(Status.INCOMPLETE);

        OrderTray orderTray = new OrderTray();
        if (oi != null) {
            persistence.create(item);
            orderTray.setId(item.getId());
            orderTray.setAddress(oi.getDeliveryAddress());
            orderTray.setKiosk(oi.getKiosk().getId());

            List<Cart> carts = persistence.getAllCartsByBatchId(oi.getBatchId());
            List<TrayItem> tis = new ArrayList<>();

            for (Cart c : carts) {
                Item i;
                if (c.getItemVariation() != null) {
                    i = persistence.find(Item.class, c.getItemVariation().getItem().getId());
                } else {
                    i = persistence.find(Item.class, c.getItem().getId());
                }

                if (i.isStatus()) {
                    KioskItem ki = persistence.getKioskItemByKioskAndItem(oi.getKiosk().getId(), i.getId());
                    orderTray.setPlace(ki.getKiosk());
                    if (ki.isStatus()) {
                        TrayItem ti = new TrayItem();
                        ti.setQuantity(c.getQuantity().intValue());
                        ti.setInstruction(c.getMessage());

                        Food fs = hc.kioskItemToFood(ki);
//                        fs.setId(ki.getId());
//                        fs.setDescription(i.getDescription());
//                        fs.setPrice(i.getPrice());
//                        fs.setName(i.getName());
//                        fs.setCurrency(OrderedItem.Currency.Naira.getCurrency());
//                        fs.setAvailable(ki.isStatus());
//                        fs.setDateAdded(ki.getDateAdded());
//                        fs.setImageUrl(persistence.getItemImage(i).getImageUrl());
                        // fs.setMax(i.getQuantity());
                        ti.setFood(fs);

                        if (c.getItemVariation() != null) {
                            FoodVariation fv = new FoodVariation();
                            fv.setId(c.getItemVariation().getId());
                            fv.setDescription(c.getItemVariation().getDescription());
                            fv.setPrice(c.getItemVariation().getPrice());
                            fv.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                            ti.setVariation(fv);
                        }

                        List<SubItemCart> sics = persistence.getAllSubItemCartByCartId(c.getId());
                        List<Food> subItems = new ArrayList<>();
                        for (SubItemCart sic : sics) {
                            if (sic.getSubItem().isActive()) {
                                Food f = new Food();
                                f.setName(sic.getSubItem().getName());
                                f.setPrice(sic.getSubItem().getPrice());
                                f.setId(sic.getSubItem().getId());
                                f.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                                subItems.add(f);
                            }
                        }
                        ti.setSubItems(subItems);
                        tis.add(ti);
                    }
                }
            }
            orderTray.setItems(tis);
        }
        return orderTray;
    }

    @GET
    @Path("isKitchenOrRestaurant")
    @Produces(MediaType.APPLICATION_JSON)
    public Response isKitchenOrRestaurant(@QueryParam("kioskId") String kioskId) {
        Kiosk k = persistence.find(Kiosk.class, Long.valueOf(kioskId));
        String kioskType = "";
        if (k != null) {
            kioskType = k.getType().getType();
        }
        return Response.ok(new StringBuilder(kioskType)).build();
    }

    @GET
    @Path("getBusinessData")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBusinessData(@QueryParam("kioskId") String kioskId) {
        Kiosk k = persistence.find(Kiosk.class, Long.valueOf(kioskId));
        Business b;
        KioskViewMode kvm = new KioskViewMode();
        if (k != null) {
            b = k.getBusiness();

            kvm.setDescription(b.getDescription());
            kvm.setId(b.getId());
            kvm.setLogo(b.getLogoName());
            kvm.setName(b.getName());
        }
        return Response.ok(kvm).build();
    }

    @POST
    @Path("unLockCoupon")
    @Produces(MediaType.APPLICATION_JSON)
    public Response unLockCoupon(@QueryParam("trayId") Long trayId) {
        OrderedItem oi = persistence.find(OrderedItem.class, trayId);
        Map<String, String> result = new HashMap<>();
        if (oi != null) {
            try {
//                PaymentTransaction pt = persistence.getPaymentTransactionByOrder(oi);
//                if (pt != null) {
//                    persistence.delete(pt);
//                 }
                List<PaymentTransaction> pts = persistence.getPaymentTransactionsByOrder(oi);
                for (PaymentTransaction pt : pts) {
                    persistence.delete(pt);
                }

                List<OrderedItemStatusLog> logs = persistence.getOrderLog(oi.getId());
                for (OrderedItemStatusLog log : logs) {
                    persistence.delete(log);
                }

                List<Cart> cs = persistence.getAllCartsByBatchId(oi.getBatchId());
                List<SubItemCart> sscs = new ArrayList<>();
                for (Cart c : cs) {
                    sscs.addAll(persistence.getAllSubItemCartByCartId(c.getId()));
                }

                for (SubItemCart sic : sscs) {
                    persistence.delete(sic);
                }
                for (Cart c : cs) {
                    persistence.delete(c);
                }

                if (oi.getPromoCode() != null) {
                    Coupon c = persistence.getCouponByCode(oi.getPromoCode());
                    if (c != null) {
                        CouponLog cl = persistence.getCouponLogByOrderIdAndCouponId(oi.getId(), c.getId());
                        persistence.delete(cl);
                    }
                }
                persistence.delete(oi);
                result.put("result", "sucessful");
                return Response.ok(result).build();
            } catch (Exception e) {
                e.printStackTrace();
                result.put("result", "failed");
                return Response.ok(result).build();
            }

        } else {
            result.put("result", "failed");
            return Response.ok(result).build();
        }

    }

    @POST
    @Path("generatePromoCode")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generatePromoCode() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 29);
        Date expireDate = cal.getTime();
        CouponBatch cb = new CouponBatch();
        cb.setActive(true);
        cb.setBusiness(persistence.getBusinessByName("Sauce Factory"));
        cb.setDateGenerated(new Date());
        cb.setDateExpired(expireDate);
        cb.setMaxAmount(1000.0);
        cb.setMultipleUser(false);
        cb.setPercentage(10.0);
        cb.setPlatform(false);
        cb.setUsageLimit(1);
        cb.setDescription("10% OFF on all orders to the tune of NGN1000.00");
        persistence.create(cb);
        for (int count = 1; count <= 10; count++) {
            String code = UUID.randomUUID().toString().substring(0, 6).toUpperCase();
            Coupon c = new Coupon();
            c.setActive(true);
            c.setCode(code);
            c.setUsed(false);
            c.setCouponBatch(cb);
            persistence.create(c);
        }

        return Response.ok().build();
    }

    @GET
    @Path("testing")
    @Produces(MediaType.APPLICATION_JSON)
    public Response testing(@QueryParam("address") String address) {
        GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBBPFRD9W0ohojPZzSk-1fUiN3UvXaQTyQ");
        GeocodingResult[] results = null;
        try {
            results = GeocodingApi.geocode(context, address).await();
        } catch (Exception ex) {
            Logger.getLogger(KioskService.class.getName()).log(Level.SEVERE, null, ex);
        }
        LatLng latLng = results[0].geometry.location;

        System.out.println(results[0].formattedAddress);
        return Response.ok(latLng).build();
    }

    @Path("tester")
    @Produces(MediaType.APPLICATION_JSON)
    public Response tester(@QueryParam("address") String address) {
        GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBBPFRD9W0ohojPZzSk-1fUiN3UvXaQTyQ");
        GeocodingResult[] results = null;
        try {
            results = GeocodingApi.geocode(context, address).await();
        } catch (Exception ex) {
            Logger.getLogger(KioskService.class.getName()).log(Level.SEVERE, null, ex);
        }
        LatLng latLng = results[0].geometry.location;

        System.out.println(results[0].formattedAddress);
        return Response.ok(latLng).build();
    }

}
