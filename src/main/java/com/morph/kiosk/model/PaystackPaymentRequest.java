/**
*Class Name: PaystackPaymentRequest
*Project Name: Kiosk
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Aug 25, 2016 4:02:25 PM
*(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaystackPaymentRequest implements Serializable{
    
    private String reference;
    
    private Long amount;
    
    private String email;
    
    private String callback_url;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    
    
    

}
