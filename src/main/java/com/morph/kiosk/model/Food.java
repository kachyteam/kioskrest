/**
 *Class Name: Food
 *Project Name: Kiosk
 *Developer: Onyedika Okafor (ookafor@morphinnovations.com)
 *Version Info:
 *Create Date: Aug 23, 2016 2:31:23 PM
 *(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
 *as the author and owner of this file and its contents.
 */
package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Food implements Serializable {

    private long id;

    private String currency;

    private String description;

    private String name;

    private Double price;

    private Boolean available;
    
    private Date dateAdded;
    
    private Integer max;
    
    private String imageUrl;
    
    private List<ItemViewMode> subItems;
    
    private List<FoodVariation> variation;
    
    private List<SubItemGroupViewMode> subItemGroups;
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<ItemViewMode> getSubItems() {
        return subItems;
    }

    public void setSubItems(List<ItemViewMode> subItems) {
        this.subItems = subItems;
    }

    public List<FoodVariation> getVariation() {
        return variation;
    }

    public void setVariation(List<FoodVariation> variation) {
        this.variation = variation;
    }

    public List<SubItemGroupViewMode> getSubItemGroups() {
        return subItemGroups;
    }

    public void setSubItemGroups(List<SubItemGroupViewMode> subItemGroups) {
        this.subItemGroups = subItemGroups;
    }
    
    

}
